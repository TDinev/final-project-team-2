import { Repository, createConnection } from "typeorm";
import * as bcrypt from 'bcrypt'
import { User } from "./entities/users.entity";
import { UserRole } from "../common/enums/user-roles";


const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);

  const admin = await userRepo.findOne({
    where: {
      role: 'Admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const originalPassword = 'Aa123!@#';
  const salt = await bcrypt.genSalt();
  const password = await bcrypt.hash(originalPassword, salt);

  const newAdmin: User = userRepo.create({
    firstName: 'ADMIN',
    lastName: 'ADMIN',
    email: 'ADMIN@ADMIN.COM',
    username: 'ADMIN',
    password,
    salt,
    isDeleted: false,
    role: UserRole.Admin,
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);