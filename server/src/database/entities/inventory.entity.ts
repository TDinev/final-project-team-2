import { Column, Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, BaseEntity, ManyToMany, JoinTable } from "typeorm";
import { CPEs } from "./CPEs.entity";
import { CVEs } from "./CVEs.entity";

@Entity()
export class Inventory extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  product: string;

  @Column()
  vendor: string;

  @Column()
  version: string;

  @OneToOne(() => CPEs, CPEs => CPEs.name, { eager: true })
  @JoinColumn()
  cpe: CPEs;

  @ManyToMany(() => CVEs, CVEs => CVEs.inventory, { eager: true })
  @JoinTable()
  cves: CVEs[];

  @Column({ default: false })
  vulnerable: boolean
}