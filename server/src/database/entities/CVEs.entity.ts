import { Column, Entity, PrimaryColumn, ManyToMany, BaseEntity, JoinTable } from "typeorm";
import { CPEs } from "./CPEs.entity";
import { Inventory } from "./inventory.entity";

@Entity()
export class CVEs extends BaseEntity {
  @PrimaryColumn()
  id: string;

  @ManyToMany(() => CPEs, CPEs => CPEs.cve, { cascade: true })
  @JoinTable()
  cpe: Promise<CPEs[]>;

  @ManyToMany(() => Inventory, Inventory => Inventory.cves)
  inventory: Promise<Inventory[]>;

  @Column({ type: 'longtext' })
  description: string;

  @Column({ default: null })
  severity: string;

  @Column()
  lastModifiedDate: string;
}