import { CVEs } from "./CVEs.entity";
import { Entity, Column, PrimaryColumn, ManyToMany, BaseEntity } from "typeorm";

@Entity()
export class CPEs extends BaseEntity  {

    @PrimaryColumn()
    name: string;

    @Column()
    type: string;

    @Column()
    vendor: string;

    @Column()
    product: string;

    @Column()
    version: string;

    @Column({default : null})
    update: string;

    @Column({default : null})
    edition: string;

    @Column({default : null})
    language: string;

    @Column()
    webName: string;
    
    @ManyToMany(() => CVEs, CVEs => CVEs.cpe, { eager: true })
    cve: Promise<CVEs[]>;

}