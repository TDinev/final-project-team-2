import { CVEsController } from "./cves.controller";
import { CVEsService } from "./cves.service";
import { TestingModule, Test } from "@nestjs/testing";
import { CPEs } from "../database/entities/CPEs.entity";
import { CVEdto } from "./models/cve-dto";
import { SchedulerDTO } from "./models/scheduler-dto";

describe('PostsController', () => {
    let controller: CVEsController;

    let cveService: any;

    beforeEach(async () => {
        cveService = {
            getAllCves() { return null },
            loadCVEs() { return null },
            loadLocalCVEs() { return null },
            saveCVE() { return null },
            getById() { return null },
            checkIfSetUp() { return null },
            addCronJob() { return null },
            deleteCron() { return null }
        };

        const module: TestingModule = await Test.createTestingModule({
            controllers: [CVEsController],
            providers: [
                {
                    provide: CVEsService,
                    useValue: cveService,
                },
            ],
        }).compile();

        controller = module.get<CVEsController>(CVEsController);

        jest.resetAllMocks();
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(controller).toBeDefined();
    });

    describe('getAllCves', () => {
        it('calls cveService.getAllCves once', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'getAllCves');
            //Arrange
            await controller.getAllCves();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns the result from cveService.getAllCves', async () => {
            //Act
            jest.spyOn(cveService, 'getAllCves')
                .mockResolvedValue('test');
            //Arrange
            const result = await controller.getAllCves();
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getCPE', () => {
        it('calls cveService.loadCVEs once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'loadCVEs');
            const id = 'someId';
            //Arrange
            await controller.getCPE(id);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(id);
        });

        it('returns the result from cveService.loadCVEs', async () => {
            //Act
           jest.spyOn(cveService, 'loadCVEs')
                .mockResolvedValue('test');
            const id = 'someId';
            //Arrange
            const result = await controller.getCPE(id);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getLocalCVEs', () => {
        it('calls cveService.loadLocalCVEs once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'loadLocalCVEs');
            const id = 'someId';
            //Arrange
            await controller.getLocalCVEs(id);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(id);
        });

        it('returns the result from cveService.loadLocalCVEs', async () => {
            //Act
            jest.spyOn(cveService, 'loadLocalCVEs')
                .mockResolvedValue('test');
            const id = 'someId';
            //Arrange
            const result = await controller.getLocalCVEs(id);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('addCVEs', () => {
        it('calls cveService.saveCVE once with correct parameters', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'saveCVE');
            const cpe = 'someId';
            const mockedNewCVE: CVEdto = {
                id: 'someId',
                cpe: [new CPEs()],
                description: 'description',
                severity: 'none',
                lastModifiedDate: 'never'
            }
            //Arrange
            await controller.addCVEs(cpe, mockedNewCVE);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(cpe, mockedNewCVE);
        });

        it('returns the result from cveService.saveCVE', async () => {
            //Act
            jest.spyOn(cveService, 'saveCVE')
                .mockResolvedValue('test');
            const cpe = 'someId';
            const mockedNewCVE: CVEdto = {
                id: 'someId',
                cpe: [new CPEs()],
                description: 'description',
                severity: 'none',
                lastModifiedDate: 'never'
            }
            //Arrange
            const result = await controller.addCVEs(cpe, mockedNewCVE);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getById', () => {
        it('calls cveService.getById once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'getById');
            const id = 'someId';
            //Arrange
            await controller.getById(id);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(id);
        });

        it('returns the result from cveService.getById', async () => {
            //Act
            jest.spyOn(cveService, 'getById')
                .mockResolvedValue('test');
            const id = 'someId';
            //Arrange
            const result = await controller.getById(id);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('checkIfSetUp', () => {
        it('calls cveService.checkIfSetUp once', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'checkIfSetUp');
            //Arrange
            await controller.checkIfSetUp();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns the result from cveService.checkIfSetUp', async () => {
            //Act
            jest.spyOn(cveService, 'checkIfSetUp')
                .mockResolvedValue('test');
            //Arrange
            const result = await controller.checkIfSetUp();
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('activateUpdateCVE', () => {
        it('calls cveService.addCronJob once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'addCronJob');
            const mockedData: SchedulerDTO = {
                name: 'mocked',
                hour: '1',
                minutes: '1'
            }
            //Arrange
            await controller.activateUpdateCVE(mockedData);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(mockedData);
        });

        it('returns the result from cveService.addCronJob', async () => {
            //Act
            jest.spyOn(cveService, 'addCronJob')
                .mockResolvedValue('test');
            const mockedData: SchedulerDTO = {
                name: 'mocked',
                hour: '1',
                minutes: '1'
            }
            //Arrange
            const result = await controller.activateUpdateCVE(mockedData);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('deactivateUpdateCVE', () => {
        it('calls cveService.deleteCron once', async () => {
            //Act
            const spy = jest.spyOn(cveService, 'deleteCron');
            //Arrange
            await controller.deactivateUpdateCVE();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns the result from cveService.deleteCron', async () => {
            //Act
            jest.spyOn(cveService, 'deleteCron')
                .mockResolvedValue('test');
            //Arrange
            const result = await controller.deactivateUpdateCVE();
            //Assert
            expect(result).toEqual('test');
        });
    });
})
