import { Module, HttpModule } from '@nestjs/common';
import { CVEsController } from './cves.controller';
import { CVEsService } from './cves.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CVERepository } from '../repositories/cve.repository';
import { CPERepository } from '../repositories/cpe.repository';
import { InventoryRepository } from '../repositories/inventory.repository';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([CVERepository, CPERepository, InventoryRepository]),
  ],
  controllers: [CVEsController],
  providers: [CVEsService]
})
export class CVEsModule { }
