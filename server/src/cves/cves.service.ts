import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CVERepository } from '../repositories/cve.repository';
import { CVEdto } from './models/cve-dto';
import { SchedulerRegistry } from '@nestjs/schedule';
import { SystemError } from '../common/filters/system.error';
import { CPERepository } from '../repositories/cpe.repository';
import * as cron from 'cron'
import { SchedulerDTO } from './models/scheduler-dto';
import { InventoryRepository } from '../repositories/inventory.repository';

@Injectable()
export class CVEsService {

    constructor(
        private httpService: HttpService,
        @InjectRepository(CPERepository)
        private cpeRepo: CPERepository,
        @InjectRepository(CVERepository)
        private cveRepo: CVERepository,
        @InjectRepository(InventoryRepository)
        private inventoryRepo: InventoryRepository,
        private schedulerRegistry: SchedulerRegistry
    ) { }

    async getAllCves() {
        return await this.cveRepo.find();
    }

    async getById(id: string) {
        try {
            const request = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cve/1.0/${id}`).toPromise();
            const cvesArray = await request.data.result.CVE_Items;

            return await this.parseCVEsPublic(cvesArray);
        }
        catch{
            throw new SystemError(`CVE with ID ${id} not found!`, 404);
        }
    }
    async loadCVEs(CPE: string) {
        const CPEpart = CPE.split(':').slice(3, 6).join(':');

        try {
            const request = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cves/1.0?cpeMatchString=cpe:/a:${CPEpart}&startIndex=0&resultsPerPage=20`).toPromise();
            const cvesArray = await request.data.result.CVE_Items;
            return await this.parseCVEs(cvesArray);
        }
        catch {
            throw new SystemError(`Did not receive a timely response from the upstream server.`, 504);
        }
    }

    async loadLocalCVEs(CPE: string) {
        const query = await this.cveRepo.createQueryBuilder("CVEs")
            .leftJoinAndSelect("CVEs.cpe", "cpe")
            .where('cpe.name = :name', { name: CPE })
            .getMany();
        return query.map(item => ({
            id: item.id,
            description: item.description,
            severity: item.severity,
            lastModifiedDate: item.lastModifiedDate
        }));
    }

    async parseCVEs(cvesArray) {
        return cvesArray.map(cve => ({
            id: cve.cve.CVE_data_meta.ID,
            description: cve.cve.description.description_data[0].value,
            severity: cve.impact ? cve.impact.baseMetricV2.severity : null,
            lastModifiedDate: cve.lastModifiedDate
        }));
    }

    async parseCVEsPublic(cvesArray) {
        const result = [];

        for (const cve of cvesArray) {
            const id = cve.cve.CVE_data_meta.ID;
            const description = cve.cve.description.description_data[0].value;
            const severity = cve.impact.baseMetricV2.severity;
            const lastModifiedDate = cve.lastModifiedDate;
            const cpes = [];

            if (cve.configurations) {

                for (const node of cve.configurations.nodes) {

                    if (node.children) {
                        for (const child of node.children) {
                            for (const cpe of child.cpe_match) {

                                if (cpe.vulnerable == true) {
                                    cpes.push(cpe.cpe23Uri);
                                }
                            }
                        }
                    } else {

                        for (const match of node.cpe_match) {
                            if (match.vulnerable == true) {
                                cpes.push(match.cpe23Uri);
                            }
                        }
                    }
                }
            }
            result.push(({ id, description, severity, lastModifiedDate, cpes }))
        }

        return result;
    }

    async saveCVE(CPE: string, cve: CVEdto) {
        const CPEEntity = await this.cpeRepo.findOne({ name: CPE });
        const inventory = await this.inventoryRepo.findOne({ cpe: CPEEntity });
        await this.inventoryRepo.setVulnerable(inventory);
        return await this.cveRepo.saveCVE(CPEEntity, inventory, cve);
    }

    async addCronJob(data: SchedulerDTO) {
        const { name, hour, minutes } = data;

        const job = new cron.CronJob(`${minutes} ${hour} * * *`, async () => {
            const today = new Date().toISOString().slice(0, 10);
            try {
                const update = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cves/1.0?modStartDate=${today}T00:00:00:000%20UTC-04:00&startIndex=0&resultsPerPage=500`).toPromise();
                const cvesArray = await update.data.result.CVE_Items;

                const parsed = await this.parseCVEs(cvesArray);
                for (const cve of parsed) {
                    const savedCVE = await this.cveRepo.findOne({ id: cve.id });
                    if (savedCVE) {
                        this.cveRepo.updateCVE(savedCVE, cve);
                    }
                }
            } catch{
                throw new SystemError(`Did not receive a timely response from the upstream server.`, 504);
            }
        });

        this.schedulerRegistry.addCronJob(name, job);
        job.start();
    }

    async deleteCron() {
        this.schedulerRegistry.deleteCronJob('updateCVEs');
    }

    async checkIfSetUp() {
        const setUp = this.schedulerRegistry.getCronJobs();

        if (setUp.size > 0) return true;
        else return false;
    }

    async manualUpdate() {
        const today = new Date().toISOString().slice(0, 10);

        try {
            const update = await this.httpService.get(`https://services.nvd.nist.gov/rest/json/cves/1.0?modStartDate=${today}T00:00:00:000%20UTC-04:00&startIndex=0&resultsPerPage=500`).toPromise();
            const cvesArray = await update.data.result.CVE_Items;
            const parsed = await this.parseCVEs(cvesArray);

            for (const cve of parsed) {
                const savedCVE = await this.cveRepo.findOne({ id: cve.id });
                if (savedCVE) {
                    this.cveRepo.updateCVE(savedCVE, cve);
                }
            }
        } catch{
            throw new SystemError(`Did not receive a timely response from the upstream server.`, 504);
        }
    }
}
