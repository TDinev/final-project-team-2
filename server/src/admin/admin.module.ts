import { AdminController } from "./admin.controller";
import { AdminService } from "./admin.service";
import { HttpModule, Module } from "@nestjs/common";
import { CVEsService } from "../cves/cves.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserRepository } from "../repositories/user.repository";
import { CVERepository } from "../repositories/cve.repository";
import { JwtStrategy } from "../users/jwt.strategy";
import { PassportModule } from "@nestjs/passport";
import { CPERepository } from "../repositories/cpe.repository";
import { InventoryRepository } from "../repositories/inventory.repository";

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    HttpModule,
    TypeOrmModule.forFeature([UserRepository, CVERepository, CPERepository, InventoryRepository])
  ],
  controllers: [AdminController],
  providers: [AdminService, CVEsService, JwtStrategy,
  ]
})
export class AdminModule { }