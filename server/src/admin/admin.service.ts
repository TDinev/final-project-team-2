import { Injectable } from "@nestjs/common";
import { CreateUserDto } from "../users/dto/createUserDto";
import { User } from "../database/entities/users.entity";
import * as bcrypt from 'bcrypt';
import { ShowUserDto } from "../users/dto/showUserDto";
import { plainToClass } from "class-transformer";
import { UserRepository } from "../repositories/user.repository";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class AdminService {

    constructor(
        @InjectRepository(UserRepository)
        private userRepo: UserRepository
    ) { }

    async signUpUser(userData: CreateUserDto): Promise<ShowUserDto>{
        const newUser = new User();
        newUser.firstName = userData.firstName;
        newUser.lastName = userData.lastName;
        newUser.email = userData.email;
        newUser.username = userData.username;
        newUser.salt = await bcrypt.genSalt();
        newUser.password = await this.hashPassword(userData.password, newUser.salt);
        await newUser.save()
        return plainToClass(ShowUserDto, newUser, {excludeExtraneousValues: true});
    }

    async removeUser(id:number){
        const foundUser = await this.userRepo.findOne({id});
        foundUser.isDeleted = true;
        await foundUser.save();
    }
    
    private async hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
    }
}
