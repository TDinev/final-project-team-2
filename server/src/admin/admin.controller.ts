import { Controller, Post, Body, Param, Delete, Get, UseGuards } from "@nestjs/common";
import { AdminService } from "./admin.service";
import { CreateUserDto } from '../users/dto/createUserDto';
import { CVEsService } from "../cves/cves.service";
import { AdminGuard } from "../common/guards/admin.guard";
import { AuthGuard } from "@nestjs/passport";


@Controller('admin')
@UseGuards(AuthGuard(), AdminGuard)
export class AdminController {

    constructor(
        private readonly adminService: AdminService,
        private readonly cveService: CVEsService,
    ) {}

    @Post('/signup')
    async signUpUser(
        @Body() userData: CreateUserDto){
        return this.adminService.signUpUser(userData);
    }

    @Delete('/users/:id')
    async removeUser(
        @Param('id') id: number){
        return this.adminService.removeUser(id);
    }

    @Get('/cves/refresh')
    async manualCVEupdate(){
        return this.cveService.manualUpdate();
    }
}
