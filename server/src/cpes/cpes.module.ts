import { Module, HttpModule } from "@nestjs/common";
import { CPEsService } from "./cpes.service";
import { CPEsController } from "./cpes.controller";
import { CPERepository } from "../repositories/cpe.repository";
import { TypeOrmModule } from "@nestjs/typeorm";
import { InventoryRepository } from '../repositories/inventory.repository';
import { PassportModule } from "@nestjs/passport";

@Module({
  imports: [
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([
      CPERepository,
      InventoryRepository,
    ])
  ],
  controllers: [
    CPEsController
  ],
  providers: [
    CPEsService
  ],
  exports: [
  ],
})
export class CPEsModule { }
