import { CPERepository } from "../repositories/cpe.repository";
import { InventoryRepository } from "../repositories/inventory.repository";
import { TestingModule, Test } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { HttpService } from "@nestjs/common";
import { SchedulerRegistry } from "@nestjs/schedule";
import { CPEsService } from './cpes.service';

describe('CPEsService', () => {

    let service: CPEsService;
    let http: any;
    let cpesRepo: any;
    let inventoryRepo: any;
    let scheduler: any;

    beforeEach(async () => {
        http = {
            get() { return null; },
        };

        cpesRepo = {
            count() { return null; },
            saveCPE() { return null; },
            suggestedCpes() { return null; },
            getRelevantCPEs() { return null; },
            getAllProductNames() { return null; },
            getAllVendorNames() { return null; },
            checkIfProductExists() { return null; },
            checkIfVendorExists() { return null; },
            checkIfCpeExists() { return null; },
        };

        inventoryRepo = {
            findOne() { return null; },
        };

        scheduler = {
            deleteCronJob() { return null; },
            getCronJobs() { return null; },
        };

        const module: TestingModule = await Test.createTestingModule({
            providers: [
                CPEsService,
                { provide: getRepositoryToken(CPERepository), useValue: cpesRepo },
                { provide: getRepositoryToken(InventoryRepository), useValue: inventoryRepo },
                { provide: HttpService, useValue: http },
                { provide: SchedulerRegistry, useValue: scheduler }
            ],
        }).compile();

        cpesRepo = module.get<CPERepository>(CPERepository);
        inventoryRepo = module.get<InventoryRepository>(InventoryRepository);
        http = module.get<HttpService>(HttpService);
        scheduler = module.get<SchedulerRegistry>(SchedulerRegistry);
        service = module.get<CPEsService>(CPEsService);
        jest.resetAllMocks();
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('initialLoadCPEsAPI()', () => {
        it('calls cpeRepo.find once', async () => {
            //Act
            const spy = jest.spyOn(http, 'get');
            const i = 0;
            const url = `https://services.nvd.nist.gov/rest/json/cpes/1.0?startIndex=${i}&resultsPerPage=5000`;
            //Arrange
            service.initialLoadCPEsAPI();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(url);
        });
    });

    describe('deleteCron', () => {
        it('calls scheduler.deleteCronJob once', async () => {
            //Act
            const spy = jest.spyOn(scheduler, 'deleteCronJob');
            //Arrange
            service.deleteCron();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('saveCPEs', () => {
        it('calls cpesRepo.saveCPE twice', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'saveCPE');
            const cpesArray = [{cpe23Uri: '12345678a'}, {cpe23Uri: '12345678a'}];
            service.saveCPEs(cpesArray);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(cpesArray[0].cpe23Uri);
        });
    });
    describe('getAllRelevantCPEs', () => {
        it ('calls cpesRepo.initialLoadCPEsAPI once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'getRelevantCPEs');
            const vendor = 'vendor';
            const productName = 'product';
            //Arrange
            await service.getAllRelevantCPEs(vendor, productName);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(vendor, productName);
        });

        it ('returns the result from cpeRepo.getAllRelevantCPEs', async () => {
            //Act
            jest.spyOn(cpesRepo, 'getRelevantCPEs')
            .mockResolvedValue('test');
            const vendor = 'vendor';
            const productName = 'product';
            //Arrange
            const result = await service.getAllRelevantCPEs(vendor, productName);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getSuggestedCpes', () => {
        it ('calls inventoryRepo.findOne once', async () => {
            //Act
            const foundProduct = {
                id: 1,
                product: 'product',
                vendor: 'vendor',
                version: 'version',
            }
            const spy = jest.spyOn(inventoryRepo, 'findOne')
            .mockResolvedValue(foundProduct);
            const inventoryItemId = 1 ;
            const filter = { id: inventoryItemId };
            //Arrange
            await service.getSuggestedCpes(inventoryItemId);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(filter);
        });
        
        it ('calls cpesRepo.suggestedCpes once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'suggestedCpes');
            const inventoryItemId = 1 ;
            const foundProduct = {
                id: 1,
                product: 'product',
                vendor: 'vendor',
                version: 'version',
            }
            jest.spyOn(inventoryRepo, 'findOne')
            .mockResolvedValue(foundProduct);
            const vendor = foundProduct.vendor;
            const product = foundProduct.product;
            const version = foundProduct.version;
            const cpeArray = [{
                name: 'abc',
                type: 'abc',
                vendor: 'abc',
                product: 'abc',
                version: 'abc',
                update: 'abc',
                edition: 'abc',
                language: 'abc',
            }]
            jest.spyOn(service, 'getAllRelevantCPEs')
            .mockResolvedValue(cpeArray);
            //Arrange
            await service.getSuggestedCpes(inventoryItemId);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(cpeArray, vendor, product, version);
        });
        it ('returns the result from cpesRepo.suggestedCpes', async () => {
            //Act
            jest.spyOn(cpesRepo, 'suggestedCpes')
            .mockResolvedValue('test');
            const inventoryItemId = 1 ;
            const foundProduct = {
                id: 1,
                product: 'product',
                vendor: 'vendor',
                version: 'version',
            }
            jest.spyOn(inventoryRepo, 'findOne')
            .mockResolvedValue(foundProduct);
            const cpeArray = [{
                name: 'abc',
                type: 'abc',
                vendor: 'abc',
                product: 'abc',
                version: 'abc',
                update: 'abc',
                edition: 'abc',
                language: 'abc',
            }]
            jest.spyOn(service, 'getAllRelevantCPEs')
            .mockResolvedValue(cpeArray);
            //Arrange
            const result = await service.getSuggestedCpes(inventoryItemId);
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('checkIfEmpty', () => {
        it('calls cpesRepo.count once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'count');
            //Arrange
            service.checkIfEmpty();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        }); 
    });
    describe('getAllProductNames', () => {
        it('calls cpesRepo.getAllProductNames once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'getAllProductNames');
            //Arrange
            service.getAllProductNames();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        }); 
        it('returns the result from cpesRepo.getAllProductNames', async () => {
            //Act
            jest.spyOn(cpesRepo, 'getAllProductNames')
            .mockResolvedValue('test');
            //Arrange
            const result = await service.getAllProductNames();
            //Assert
            expect(result).toEqual('test');
        }); 
    });    
    describe('getAllVendorNames', () => {
        it('calls cpesRepo.getAllVendorNames once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'getAllVendorNames');
            //Arrange
            service.getAllVendorNames();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        }); 
        it('returns the result from cpesRepo.getAllVendorNames', async () => {
            //Act
            jest.spyOn(cpesRepo, 'getAllVendorNames')
            .mockResolvedValue('test');
            //Arrange
            const result = await service.getAllVendorNames();
            //Assert
            expect(result).toEqual('test');
        }); 
    });
    describe('checkIfProductExists', () => {
        it('calls cpesRepo.checkIfProductExists once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'checkIfProductExists');
            const productName = 'product'
            //Arrange
            service.checkIfProductExists(productName);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(productName);
        }); 
        it('returns the result from cpesRepo.checkIfProductExists', async () => {
            //Act
            jest.spyOn(cpesRepo, 'checkIfProductExists')
            .mockResolvedValue('test');
            const productName = 'product'
            //Arrange
            const result = await service.checkIfProductExists(productName);
            //Assert
            expect(result).toEqual('test');
        }); 
    });
    describe('checkIfVendorExists', () => {
        it('calls cpesRepo.checkIfVendorExists once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'checkIfVendorExists');
            const vendorName = 'vendor'
            //Arrange
            service.checkIfVendorExists(vendorName);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(vendorName);
        }); 
        it('returns the result from cpesRepo.checkIfVendorExists', async () => {
            //Act
            jest.spyOn(cpesRepo, 'checkIfVendorExists')
            .mockResolvedValue('test');
            const vendorName = 'vendor'
            //Arrange
            const result = await service.checkIfVendorExists(vendorName);
            //Assert
            expect(result).toEqual('test');
        }); 
    });
    describe('checkIfCpeExists', () => {
        it('calls cpesRepo.checkIfCpeExists once', async () => {
            //Act
            const spy = jest.spyOn(cpesRepo, 'checkIfCpeExists');
            const vendorName = 'vendor';
            const productName = 'product';
            const version = 'version';
            //Arrange
            service.checkIfCpeExists(vendorName, productName, version);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(vendorName, productName, version);
        }); 
        it('returns the result from cpesRepo.checkIfCpeExists', async () => {
            //Act
            jest.spyOn(cpesRepo, 'checkIfCpeExists')
            .mockResolvedValue('test');
            const vendorName = 'vendor';
            const productName = 'product';
            const version = 'version';
            //Arrange
            const result = await service.checkIfCpeExists(vendorName, productName, version);
            //Assert
            expect(result).toEqual('test');
        }); 
    });
});