export class SearchByQueryDTO {
    product?: string;
    vendor?: string;
    cpe?: string;
}