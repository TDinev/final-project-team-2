import { CPEsController } from './cpes.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { SchedulerDTO } from '../cves/models/scheduler-dto';
import { CPEsService } from './cpes.service';


describe('CPEsController', () => {
    let controller: CPEsController;
  
    let cpesService: any;
  
    beforeEach(async () => {
        cpesService = {
            initialLoadCPEsAPI() { return null; },
            addCronJob() { return null; },
            deleteCron() { return null; },
            saveCPEs() { return null; },
            getAllRelevantCPEs() { return null; },
            getSuggestedCpes() { return null; },
            downloadCPEs() { return null; },
            xmlParser() { return null; },
            handleZippedFile() { return null; },
            initialLoadCPEsFile() { return null; },
            checkIfEmpty() { return null; },
            getAllProductNames() { return null; },
            getAllVendorNames() { return null; },
            checkIfProductExists() { return null; },
            checkIfVendorExists() { return null; },
            checkIfCpeExists() { return null; }
      };
  
      const module: TestingModule = await Test.createTestingModule({
        controllers: [CPEsController],
        providers: [
          {
            provide: CPEsService,
            useValue: cpesService,
          },
        ],
      }).compile();
  
      controller = module.get<CPEsController>(CPEsController);
    });
  
    it('should be defined', () => {
      // Arrange & Act & Assert
      expect(controller).toBeDefined();
    });

    describe('getCPEApi', () => {
        it ('calls cpeService.initialLoadCPEsAPI once', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'initialLoadCPEsAPI');
            //Arrange
            await controller.getCPEApi();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it ('returns the result from cpeService.initialLoadCPEsAPI', async () => {
            //Act
            jest.spyOn(cpesService, 'initialLoadCPEsAPI')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.getCPEApi();
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('getCPEFile', () => {
        it ('calls cpeService.initialLoadCPEsFile once', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'initialLoadCPEsFile');
            //Arrange
            await controller.getCPEFile();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it ('returns the result from cpeService.initialLoadCPEsFile', async () => {
            //Act
            jest.spyOn(cpesService, 'initialLoadCPEsFile')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.getCPEFile();
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('getAllProductNames', () => {
        it ('calls cpeService.getAllProductNames once', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'getAllProductNames');
            //Arrange
            await controller.getAllProductNames();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it ('returns the result from cpeService.getAllProductNames', async () => {
            //Act
            jest.spyOn(cpesService, 'getAllProductNames')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.getAllProductNames();
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('getAllVendorNames', () => {
        it ('calls cpeService.getAllVendorNames once', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'getAllVendorNames');
            //Arrange
            await controller.getAllVendorNames();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it ('returns the result from cpeService.getAllVendorNames', async () => {
            //Act
            jest.spyOn(cpesService, 'getAllVendorNames')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.getAllVendorNames();
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('checkIfProductExists', () => {
        it ('calls cpesService.checkIfProductExists once with correct product', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'checkIfProductExists');
            const product = 'test';
            //Arrange
            await controller.checkIfProductExists(product);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(product);
        });
        it ('returns the result from cpeService.checkIfProductExists', async () => {
            //Act
            jest.spyOn(cpesService, 'checkIfProductExists')
            .mockResolvedValue(1);
            const product = 'test';
            //Arrange
            const result = await controller.checkIfProductExists(product);
            //Assert
            expect(result).toEqual(1);
        });
    });
    describe('checkIfVendorExists', () => {
        it ('calls cpesService.checkIfVendorExists once with correct product', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'checkIfVendorExists');
            const product = 'test';
            //Arrange
            await controller.checkIfVendorExists(product);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(product);
        });
        it ('returns the result from cpeService.checkIfVendorExists', async () => {
            //Act
            jest.spyOn(cpesService, 'checkIfVendorExists')
            .mockResolvedValue(1);
            const product = 'test';
            //Arrange
            const result = await controller.checkIfVendorExists(product);
            //Assert
            expect(result).toEqual(1);
        });
    });
    describe('getSuggestedCpes', () => {
        it ('calls cpesService.getSuggestedCpes once with correct id', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'getSuggestedCpes');
            const id = 1;
            //Arrange
            await controller.getSuggestedCpes(id);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(id);
        });
        it ('returns the result from cpeService.getSuggestedCpes', async () => {
            //Act
            jest.spyOn(cpesService, 'getSuggestedCpes')
            .mockResolvedValue('test');
            const id = 1;
            //Arrange
            const result = await controller.getSuggestedCpes(id);
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('checkIfCpeExists', () => {
        it ('calls cpesService.checkIfCpeExists once with correct vendorName, productName, version', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'checkIfCpeExists');
            const vendorName = 'vendor'
            const productName = 'product'
            const version = 'version'
            //Arrange
            await controller.checkIfCpeExists(vendorName, productName, version);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(vendorName, productName, version);
        });
        it ('returns the result from cpeService.getSuggestedCpes', async () => {
            //Act
            jest.spyOn(cpesService, 'checkIfCpeExists')
            .mockResolvedValue('test');
            const vendorName = 'vendor'
            const productName = 'product'
            const version = 'version'
            //Arrange
            const result =  await controller.checkIfCpeExists(vendorName, productName, version);
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('checkIfEmpty', () => {
        it ('calls cpesService.checkIfEmpty once with correct filter', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'checkIfEmpty');
            //Arrange
            await controller.checkIfEmpty();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith();
        });

        it ('returns the result from cpesService.checkIfEmpty', async () => {
            //Act
            jest.spyOn(cpesService, 'checkIfEmpty')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.checkIfEmpty();
            //Assert
            expect(result).toEqual('test');
        });
    });
    describe('activateUpdateCPE', () => {
        it('calls cpesService.addCronJob once with correct parameter', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'addCronJob');
            const mockedData: SchedulerDTO = {
                name: 'mocked',
                hour: '1',
                minutes: '1'
            }
            //Arrange
            await controller.activateUpdateCPE(mockedData);
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(mockedData);
        });

        it('returns the result from cpesService.addCronJob', async () => {
            //Act
            jest.spyOn(cpesService, 'addCronJob')
            .mockResolvedValue('test');
            const mockedData: SchedulerDTO = {
                name: 'mocked',
                hour: '1',
                minutes: '1'
            }
            //Arrange
            const result = await controller.activateUpdateCPE(mockedData);
            //Assert
            expect(result).toEqual('test');
        });
    });

    describe('deactivateUpdateCPE', () => {
        it('calls cpesService.deleteCron once', async () => {
            //Act
            const spy = jest.spyOn(cpesService, 'deleteCron');
            //Arrange
            await controller.deactivateUpdateCPE();
            //Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('returns the result from cpesService.deleteCron', async () => {
            //Act
            jest.spyOn(cpesService, 'deleteCron')
            .mockResolvedValue('test');
            //Arrange
            const result = await controller.deactivateUpdateCPE();
            //Assert
            expect(result).toEqual('test');
        });
    });
});