import * as nodemailer from 'nodemailer';
import * as config from 'config';

export const sendEmail = async (email: string, body: string) => {
  const transporter = nodemailer.createTransport({
    host: config.get('mailer.server'),
    port: config.get('mailer.port'),
    secure: true,
    auth: {
      user: config.get('mailer.username'),
      pass: config.get('mailer.password'),
    }
  });

  await transporter.sendMail({
    from: '"Xposed" <xposed@abv.bg>',
    to: email,
    subject: "New assignments",
    text: `${body}`,
    html: `<p> ${body} <p>`
  });

}
