import { CPEs } from "../database/entities/CPEs.entity";
import { EntityRepository, Repository } from "typeorm";
import { CpeParser } from "@thefaultvault/tfv-cpe-parser"
import { CpeDto } from '../cpes/dto/cpeDto';
import * as fs from 'fs';

@EntityRepository(CPEs)
export class CPERepository extends Repository<CPEs> {

    async saveCPE(name: string) {

        const parser = new CpeParser();
        const parsed = parser.parse(name);
        const specialCharactersObject = { '[,]': '%2c', '[(]': '%28', '[)]': '%29', '[|]': '%7c' }
        const CPE = new CPEs();
        CPE.name = name;
        CPE.type = parsed.part;
        CPE.vendor = parsed.vendor;
        CPE.product = parsed.product;
        CPE.version = parsed.version;
        CPE.update = parsed.update;
        CPE.edition = parsed.edition;
        CPE.language = parsed.language;
        CPE.webName = `"cpe:2.3:${parsed.part}:${parsed.vendor}}:${parsed.product}:${parsed.version}"`.specialCharactersReplace(specialCharactersObject);
        await CPE.save();
    }

    async getRelevantCPEs(vendor: string, productName: string): Promise<CpeDto[]> {
        const query = this.createQueryBuilder("cpes")
            .where("cpes.vendor = :vendor", { vendor: vendor })
            .orWhere("cpes.product = :productName", { productName: productName })
            .orderBy("cpes.product", "DESC")
        const cpesArray = await query.getMany();
        return cpesArray
    }

    private weightedSearch(array: CpeDto[], weightedTests, sortProperty: string): CpeDto[] {
        return array.map((cpe: CpeDto) => {
            return {
                element: cpe,
                weight: weightedTests.map(weightedTest => {
                    const testResult = weightedTest.test(cpe);
                    return testResult * weightedTest.weight;
                }).reduce((previousValue, currentValue) => {
                    return previousValue + currentValue;
                }, 0)
            };
        }).filter((element) => {
            return element.weight > 0;
        }).sort((obj1, obj2) => {
            return obj2.weight - obj1.weight || obj2.element[sortProperty].replace(/\s/g, '') - obj1.element[sortProperty].replace(/\s/g, '');
        }).map(function (e) {
            return e.element;
        });
    }

    suggestedCpes(cpeArray: CpeDto[], vendor: string, product: string, version: string): CpeDto[] {
        const result = this.weightedSearch(cpeArray, [
            { test: function (testElement: CpeDto) { return testElement.product.toLowerCase().localeCompare(product) === 0; }, weight: 2 },
            { test: function (testElement: CpeDto) { return testElement.vendor.toLowerCase().localeCompare(vendor) === 0; }, weight: 1 },
            { test: function (testElement: CpeDto) { return testElement.version.toLowerCase().indexOf(version) >= 0; }, weight: 0.5 }
        ], "vendor");
        return result.slice(0, 50);
    }

    async cpeQuery() {
        const csvPath = 'cpes.csv'
        await this.manager.query(`LOAD DATA LOCAL INFILE '${csvPath}' INTO TABLE cp_es FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES`)
        fs.unlink(`cpes.csv`, (err) => {
            if (err) throw err;
        });
    }

    async getAllProductNames() {
        return await this.manager.query(`SELECT DISTINCT product FROM cp_es ORDER BY product`)
    }

    async getAllVendorNames() {
        return await this.manager.query(`SELECT DISTINCT vendor FROM cp_es ORDER BY vendor`)
    }

    async checkIfProductExists(productName: string): Promise<number> {
        const query = this.createQueryBuilder("cpes")
            .where("cpes.product = :productName", { productName: productName })
        const numberOfResults = await query.getCount();
        return numberOfResults
    }

    async checkIfVendorExists(vendorName: string): Promise<number> {
        const query = this.createQueryBuilder("cpes")
            .where("cpes.vendor = :vendorName", { vendorName: vendorName })
        const numberOfResults = await query.getCount();
        return numberOfResults
    }

    async checkIfCpeExists(vendorName: string, productName: string, version: string): Promise<CpeDto> {
        const query = this.createQueryBuilder("cpes")
            .where("cpes.vendor = :vendorName", { vendorName: vendorName })
            .andWhere("cpes.product = :productName", { productName: productName })
            .andWhere("cpes.version = :version", { version: version })
        const cpe = await query.getOne();
        return cpe
    }

}