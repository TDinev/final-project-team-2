import { EntityRepository, Repository } from "typeorm";
import { CVEs } from "../database/entities/CVEs.entity";
import { CVEdto } from "../cves/models/cve-dto";
import { CPEs } from "../database/entities/CPEs.entity";


@EntityRepository(CVEs)
export class CVERepository extends Repository<CVEs> {

    async saveCVE(CPE: CPEs, inventory: any, cve: CVEdto) {
        const savedCVE = await this.findOne({ where: { id: cve.id }, relations: ['cpe'] });

        if (!savedCVE) {
            const CVE = new CVEs();
            CVE.id = cve.id;
            CVE.description = cve.description;
            CVE.severity = cve.severity ? cve.severity : null;
            CVE.lastModifiedDate = cve.lastModifiedDate;
            CVE.cpe = Promise.resolve([CPE]);
            CVE.inventory = Promise.resolve([inventory]);
            await CVE.save();

        } else {
            await this.updateCVE(savedCVE, cve, inventory, CPE)
        }
    }

    async updateCVE(cve: CVEs, updated: CVEdto, inventory?, CPE?) {
        cve.description = updated.description;
        cve.lastModifiedDate = updated.lastModifiedDate;
        cve.severity = updated.severity ? updated.severity : null;
        if (inventory) {
            (await cve.inventory).push(inventory);
        }
        if (CPE) {
            (await cve.cpe).push(CPE);
        }
        await cve.save();
    }
}  