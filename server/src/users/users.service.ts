import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../repositories/user.repository';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { ShowUserDto } from './dto/showUserDto';
import { FindUserDto } from './dto/findUserDto';
import { LoginUserDto } from './dto/loginUserDto';
import { JwtPayload } from './jwt-payload.interface';
import { User } from '../database/entities/users.entity';
import { UpdateUserDto } from './dto/updateUserDto';
import * as bcrypt from 'bcrypt'
import { SystemError } from '../common/filters/system.error';
import { CreateUserDto } from './dto/createUserDto';

@Injectable()
export class UsersService {

    private logger = new Logger('UsersService');
    private blacklistedTokens: string[] = [];
    
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private jwtService: JwtService
    ) { }

    async all(): Promise<ShowUserDto[]> {
        const found = await this.userRepository.find();
        return plainToClass(ShowUserDto, found, { excludeExtraneousValues: true })
    }

    async find(options: FindUserDto): Promise<ShowUserDto[]> {
        const found = await this.userRepository.getUsers(options);
        return plainToClass(ShowUserDto, found, { excludeExtraneousValues: true })
    }

    async register(createUserDTO: CreateUserDto): Promise<ShowUserDto> {
        return this.userRepository.register(createUserDTO);
    }
    async signIn(loginUserDto: LoginUserDto): Promise<{ accessToken: string }> {
        const user = await this.userRepository.validateUserPassword(loginUserDto);

        if (!user) {
            throw new SystemError('Invalid Credentials', 401)
        }
        const id = user.id;
        const username = user.username;
        const role = user.role;
        const jwtPayload: JwtPayload = { username, id, role };
        const accessToken = this.jwtService.sign(jwtPayload);
        this.logger.verbose(`\x1b[1m\x1b[34mGenerated JWT Token with payload \x1b[4m\x1b[37m${JSON.stringify(jwtPayload)}\x1b[0m`)

        return { accessToken };
    }

    public async signOut(token: string): Promise<string> {
        this.blacklistToken(token);
        return `Successful logout!`
    }

    async update(id: number, updates: Partial<UpdateUserDto>, user: User): Promise<ShowUserDto> {
        const { password, firstName, lastName, email } = updates;
        const foundUser = await this.userRepository.findOne({ where: { id, email: user.email } });
        if (!foundUser) {
            throw new SystemError(`Unauthorized`, 401);
        }

        firstName ? foundUser.firstName = firstName : foundUser.firstName;
        lastName ? foundUser.lastName = lastName : foundUser.lastName;
        email ? foundUser.email = email : foundUser.email;
        // avatarUrl ? foundUser.avatarUrl = avatarUrl : foundUser.avatarUrl;

        if (password) {
            const salt = await bcrypt.genSalt();
            foundUser.password = await bcrypt.hash(password, salt);
        }

        const updatedUser = await this.userRepository.save(foundUser);
        return plainToClass(ShowUserDto, updatedUser, { excludeExtraneousValues: true })
    }

    public blacklistToken(token: string): void {
        this.blacklistedTokens.push(token);
    }

    public isTokenBlacklisted(token: string): boolean {
        if (this.blacklistedTokens.includes(token)) {
            return true;
        }
        return false;
    }
}
