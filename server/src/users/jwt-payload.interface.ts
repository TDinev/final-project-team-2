import { UserRole } from "../common/enums/user-roles";

export interface JwtPayload {
    username: string;
    id:number;

    role: UserRole;
}