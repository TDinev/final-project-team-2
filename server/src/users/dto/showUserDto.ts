import { UserRole } from "../../common/enums/user-roles";
import { Expose } from 'class-transformer'

export class ShowUserDto {

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose()
    username: string;

    @Expose()
    email: string;

    // @Expose()
    // avatarUrl: string;

    @Expose()
    role: UserRole;

    @Expose()
    createdDate: Date;
}