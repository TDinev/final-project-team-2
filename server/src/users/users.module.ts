import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../repositories/user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import * as config from 'config';
import { MulterModule } from '@nestjs/platform-express';
import { extname } from 'path';
import { SystemError } from '../common/filters/system.error';
import { diskStorage } from 'multer'
import { MailerController } from '../mailer/mailer.controller';
import { MailerService } from '../mailer/mailer.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || config.get('jwt.secret'),
      signOptions: {
        expiresIn: config.get('jwt.expiresIn'),
      },
    }),
    TypeOrmModule.forFeature([
      UserRepository,
    ]),
    MulterModule.register({
      fileFilter(_, file, cb) {
        const ext = extname(file.originalname);
        const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

        if (!allowedExtensions.includes(ext)) {
          return cb(
            new SystemError('Only images are allowed', 400),
            false,
          );
        }

        cb(null, true);
      },
      storage: diskStorage({
        destination: './avatars',
        filename: (_, file, cb) => {
          const randomName = Array.from({ length: 32 })
            .map(() => Math.round(Math.random() * 10))
            .join('');

          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  ],
  controllers: [
    UsersController,
    MailerController
  ],
  providers: [
    UsersService,
    JwtStrategy,
    MailerService
  ],
  exports: [
    TypeOrmModule,
    JwtStrategy,
    PassportModule,
    UsersService,
  ],
})
export class UsersModule { }
