import { Expose, Transform } from 'class-transformer';
import { CPEs } from '../../database/entities/CPEs.entity';

export class ShowProductDTO {
    @Expose()
    id: number;

    @Expose()
    product: string;

    @Expose()
    vendor: string;

    @Expose()
    version: string;

    @Expose()
    @Transform((_,obj)=> obj.cpe)
    cpe: CPEs;
}