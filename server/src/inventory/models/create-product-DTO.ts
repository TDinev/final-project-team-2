export class CreateProductDTO {
    product: string;
    vendor: string;
    version: string;
}