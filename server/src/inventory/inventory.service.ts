import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InventoryRepository } from '../repositories/inventory.repository';
import { Not } from 'typeorm';
import { Inventory } from '../database/entities/inventory.entity';
import { CreateProductDTO } from './models/create-product-DTO';
import { SystemError } from '../common/filters/system.error';
import { ShowProductDTO } from './models/show-product-DTO';
import { CPERepository } from '../repositories/cpe.repository';

@Injectable()
export class InventoryService {

    constructor(
        @InjectRepository(InventoryRepository)
        private inventoryRepo: InventoryRepository,
        @InjectRepository(CPERepository)
        private cpeRepo: CPERepository,
    ) { }

    async getAll() {
        return this.inventoryRepo.find();
    }

    async getWithCPE() {
        return this.inventoryRepo.find({ cpe: Not(null) })
    }

    async getWithoutCPE() {
        return this.inventoryRepo.find({ cpe: null })
    }

    async getById(id: number) {
        const foundProduct = await this.inventoryRepo.findOne({ id });

        if (!foundProduct) {
            throw new SystemError(`Post with ID ${id} not foundProduct!`, 404);
        }
        return {
            id: foundProduct.id,
            product: foundProduct.product,
            vendor: foundProduct.vendor,
            version: foundProduct.version,
            cpe: foundProduct.cpe,
            cves: foundProduct.cves,
        }
    }

    async deleteProduct(id: number): Promise<void> {
        const deletedProduct = await this.inventoryRepo.delete(id); // going with delete because it's cheaper than remove.
        if (deletedProduct.affected === 0) {
            throw new SystemError(`Product with ID ${id} not foundProduct!`, 404);
        }
    }

    async newProduct(data: CreateProductDTO) {
        const { vendor, product, version } = data;

        const newProduct = new Inventory();
        newProduct.vendor = vendor;
        newProduct.product = product;
        newProduct.version = version;
        await newProduct.save();

        return newProduct;
    }

    async editProduct(id: number, data: CreateProductDTO) {
        const { vendor, product, version } = data;

        const foundProduct = await this.inventoryRepo.findOne({ id })
        foundProduct.vendor = vendor;
        foundProduct.product = product;
        foundProduct.version = version;
        await foundProduct.save();

        return foundProduct;
    }

    async assignCpeToProduct(inventoryItemId: number, cpeName: string): Promise<ShowProductDTO> {
        const foundProduct = await this.inventoryRepo.findOne({ id: inventoryItemId });
        if (!foundProduct) {
            throw new SystemError(`Product with ID ${inventoryItemId} not found!`, 404);
        }
        const foundCpe = await this.cpeRepo.findOne({ name: cpeName });

        if (!foundCpe) {
            throw new SystemError(`CPE with name ${cpeName} not found!`, 404);
        }
        foundProduct.cpe = foundCpe;
        foundProduct.cves.push(...await foundCpe.cve);
        const savedProduct = await this.inventoryRepo.save(foundProduct);

        return savedProduct
    }

    async breakLinkToCpe(inventoryItemId: number): Promise<ShowProductDTO> {
        const foundProduct = await this.inventoryRepo.findOne({ id: inventoryItemId });
        if (!foundProduct) {
            throw new SystemError(`Product with ID ${inventoryItemId} not found!`, 404);
        }
        foundProduct.cpe = null;
        foundProduct.cves = null;
        const savedProduct = await this.inventoryRepo.save(foundProduct);

        return savedProduct
    }
    async getByQuery(keyword: string): Promise<ShowProductDTO[]> {

        const result = [];

        if (keyword.startsWith('*') && keyword.endsWith('*')) {
            const search = keyword.substring(1, keyword.length - 1);

            const found = await this.inventoryRepo.createQueryBuilder('inventory')
                .leftJoin('inventory.cves', 'cve')
                .where('cve.id LIKE :id', { id: '%' + search + '%' })
                .orWhere('inventory.cpeName LIKE :cpeName', { cpeName: '%' + search + '%' })
                .orWhere('inventory.product LIKE :product', { product: '%' + search + '%' })
                .orWhere('inventory.vendor LIKE :vendor', { vendor: '%' + search + '%' })
                .getMany();
            result.push(...found);

        } else if (keyword.startsWith('*')) {

            const search = keyword.substr(1);

            const found = await this.inventoryRepo.createQueryBuilder('inventory')
                .leftJoin('inventory.cves', 'cve')
                .where('cve.id LIKE :id', { id: '%' + search })
                .orWhere('inventory.cpeName LIKE :cpeName', { cpeName: '%' + search })
                .orWhere('inventory.product LIKE :product', { product: '%' + search })
                .orWhere('inventory.vendor LIKE :vendor', { vendor: '%' + search })
                .getMany();
            result.push(...found);

        } else if (keyword.endsWith('*')) {
            const search = keyword.substr(0, keyword.length - 1);

            const found = await this.inventoryRepo.createQueryBuilder('inventory')
                .leftJoin('inventory.cves', 'cve')
                .where('cve.id LIKE :id', { id: search + '%' })
                .orWhere('inventory.cpeName LIKE :cpeName', { cpeName: search + '%' })
                .orWhere('inventory.product LIKE :product', { product: search + '%' })
                .orWhere('inventory.vendor LIKE :vendor', { vendor: search + '%' })
                .getMany();

            result.push(...found);

        } else {
            const found = await this.inventoryRepo.createQueryBuilder('inventory')
                .leftJoin('inventory.cves', 'cve')
                .where('cve.id = :id', { id: keyword })
                .orWhere('inventory.cpeName = :cpeName', { cpeName: keyword })
                .orWhere('inventory.product = :product', { product: keyword })
                .orWhere('inventory.vendor = :vendor', { vendor: keyword })
                .getMany();

            result.push(...found);
        }

        if (result.length < 1) {
            throw new SystemError(`Product not found!`, 404);
        }

        return result;
    }
}
