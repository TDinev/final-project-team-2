import { Controller, Get, Post, Body, Param, Put, ParseIntPipe, UseGuards, Query, Delete } from '@nestjs/common';
import { InventoryService } from './inventory.service';
import { CreateProductDTO } from './models/create-product-DTO';
import { ShowProductDTO } from './models/show-product-DTO';
import { AuthGuard } from '@nestjs/passport';

@Controller('inventory')
@UseGuards(AuthGuard())
export class InventoryController {

    constructor(
        private readonly inventoryService: InventoryService
    ) { }

    @Get('/all')
    async getAll() {
        return await this.inventoryService.getAll();
    }

    @Get()
    async getByQuery(@Query('keyword') keyword: string): Promise<ShowProductDTO[]> {

        return await this.inventoryService.getByQuery(keyword);
    }

    @Get('/:id')
    async getById(@Param('id') id: number) {
        return await this.inventoryService.getById(id);
    }

    @Post('/new')
    async newProcuct(@Body() data: CreateProductDTO) {
        return await this.inventoryService.newProduct(data);
    }

    @Put(':id')
    async editProcuct(@Param('id') id: number, @Body() data: CreateProductDTO) {
        return await this.inventoryService.editProduct(id, data);
    }

    @Delete('/:id')
    async deleteProduct(@Param('id', ParseIntPipe) id: number): Promise<void> {
        return this.inventoryService.deleteProduct(id);
    }

    @Put('/:inventoryItemId/cpes/:cpeName')
    async assignCpeToProduct(
        @Param('inventoryItemId', ParseIntPipe) inventoryItemId: number,
        @Param('cpeName') cpeName: string,
    ): Promise<ShowProductDTO> {
        return await this.inventoryService.assignCpeToProduct(inventoryItemId, cpeName);
    }

    @Put('/:inventoryItemId/breaklink')
    async breakLinkToCpe(
        @Param('inventoryItemId', ParseIntPipe) inventoryItemId: number,
    ): Promise<ShowProductDTO> {
        return await this.inventoryService.breakLinkToCpe(inventoryItemId);
    }

}
