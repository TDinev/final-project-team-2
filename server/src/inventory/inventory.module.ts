import { Module } from '@nestjs/common';
import { InventoryController } from './inventory.controller';
import { InventoryService } from './inventory.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InventoryRepository } from '../repositories/inventory.repository';
import { CPERepository } from '../repositories/cpe.repository';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([
      InventoryRepository,
      CPERepository,
    ])
  ],
  controllers: [InventoryController],
  providers: [InventoryService]
})
export class InventoryModule { }
