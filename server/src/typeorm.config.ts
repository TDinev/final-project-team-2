import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mariadb',
    // in production host, port, username, password, database name will come from the service we use
    // or if not provided from the config files.
    host: config.get('db.host'),
    port: config.get('db.port'),
    username: config.get('db.username'),
    password: config.get('db.password'),
    database: config.get('db.database'),
    entities: [__dirname + '/../**/*.entity.js'],
    synchronize: process.env.TYPEORM_SYNC === 'true' || config.get('db.synchronize')
    // If we want to run synchronization once before using the default value.
}