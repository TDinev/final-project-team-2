import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm"
import { UsersModule } from './users/users.module';
import { InventoryModule } from './inventory/inventory.module';
import { APP_FILTER } from '@nestjs/core';
import { SystemErrorFilter } from './common/filters/system-error.filter';
import { typeOrmConfig } from './typeorm.config';
import { CPEsModule } from './cpes/cpes.module';
import { CVEsModule } from './cves/cves.module';
import { ScheduleModule } from '@nestjs/schedule';
import { AdminModule } from './admin/admin.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    ScheduleModule.forRoot(),
    UsersModule,
    AdminModule,
    InventoryModule,
    CPEsModule,
    CVEsModule
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: SystemErrorFilter
    },
  ],
})
export class AppModule { }
