import { createParamDecorator } from '@nestjs/common';
import { User } from '../../database/entities/users.entity';

// in order to get only the user info from the request. In a separate file since used a lot.
export const GetUser = createParamDecorator((data, req): User => {
    return req.user;
});
