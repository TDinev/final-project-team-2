import { Catch, ExceptionFilter, ArgumentsHost } from "@nestjs/common";
import { SystemError } from "./system.error";


@Catch(SystemError)
export class SystemErrorFilter implements ExceptionFilter {
  catch(exception: SystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    response
      .status(exception.status)
      .json({
        message: exception.message,
        statusCode: exception.status
      });
  }
}