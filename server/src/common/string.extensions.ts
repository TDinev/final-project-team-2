interface String {
    specialCharactersReplace(specialCharacters: object): string;
}

String.prototype.specialCharactersReplace = function(specialCharacters: object) {
    let cpeWebName = String(this);
    for (const i in specialCharacters) {
        cpeWebName = cpeWebName.replace(new RegExp(i, 'g'), specialCharacters[i]);
    }
    return cpeWebName;
};