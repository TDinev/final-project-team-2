import { LoginComponent } from './login.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NgForm } from '@angular/forms';
import { MaterialModule } from '../../../material.module';
import { of } from 'rxjs';
import { LoginService } from '../../login.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {

  let loginService;

  let component: LoginComponent;
  let router: Router;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach((() => {
    jest.clearAllMocks();

    loginService = {
      login() {}
    };

    TestBed.configureTestingModule({
      imports: [FormsModule, MaterialModule, RouterTestingModule.withRoutes([])],
      declarations: [LoginComponent],
      providers: [LoginService]
    })
      .overrideProvider(LoginService, { useValue: loginService })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LoginComponent);
        router = TestBed.inject(Router);
        component = fixture.componentInstance;
      });
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('onSubmit', () => {
    it('should call loginService.login() once', () => {
      // Arrange
      const testForm = {
        value: {
          email: 'ab@abv.bg',
          password: 'Asdf1234!', }
      } as NgForm;
      const spy = jest.spyOn(loginService, 'login')
        .mockReturnValue(of(testForm));
      const navigateSpy = spyOn(router, 'navigate');
      // Act
      component.onSubmit(testForm);
      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(testForm.value);
      expect(navigateSpy).toHaveBeenCalledTimes(1);
    });
});
});
