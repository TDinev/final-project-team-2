import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private registerLoginService: LoginService,
              private readonly router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(f: NgForm) {

    const loginObserver = {
      next: x => this.router.navigate(['dashboard']),
      error: err => console.error('Observer got an error: ' + err)
    };
    this.registerLoginService.login(f.value).subscribe(loginObserver);
  }

}
