import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { CreateUserDTO } from '../models/create-user-DTO';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly usersUrl = 'http://localhost:3000/users/';
  private readonly resetUrl = 'http://localhost:3000/reset/';

  private readonly helper = new JwtHelperService();
  private readonly isLoggedInSubject$: BehaviorSubject<boolean> = new BehaviorSubject(this.isUserLoggedIn());
  private readonly loggedUserSubject$: BehaviorSubject<any> = new BehaviorSubject(this.loggedUser());

  constructor(
    private http: HttpClient,
    private readonly router: Router,
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get loggedUser$(): Observable<any> {
    return this.loggedUserSubject$.asObservable();
  }

  register(model: CreateUserDTO) {
    return this.http.post(this.usersUrl + 'register', model); // add options after model
  }

  login(model: any): Observable<void> {
    return this.http.post(this.usersUrl + 'signin', model)
      .pipe(map((response: any) => {
        const user = response;
        if (user.accessToken) {
          localStorage.setItem('accessToken', user.accessToken);
          this.isLoggedInSubject$.next(true);
          this.loggedUserSubject$.next(this.loggedUser());
        }
      })
    );
  }

  passwordReset(model: any) {
    return this.http.post(this.resetUrl + 'password', model);
  }

  public logout(): void  {
    localStorage.removeItem('accessToken');
    this.isLoggedInSubject$.next(false);

    this.router.navigate(['home']);
  }

  private isUserLoggedIn(): boolean {
    return !!localStorage.getItem('accessToken');
  }

  public loggedUser(): any {
    try {
      const user = this.helper.decodeToken(localStorage.getItem('accessToken'));

      return user;
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
