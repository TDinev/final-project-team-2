import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoginService } from '../login.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: LoginService,
    private readonly router: Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {

      return this.authService.isLoggedIn$
        .pipe(
          tap(loggedIn => {
            if (!loggedIn) {
              this.router.navigate(['/home']);
            }
          }),
        );

  }
}
