import { LoginService } from '../login.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable()
export class AdminGuard implements CanActivate {
  constructor(public auth: LoginService, public router: Router) {}
  canActivate(): boolean {
    const user = this.auth.loggedUser();
    if (user.role !== 'Admin') {
      this.router.navigate(['dashboard']);
      return false;
    }
    return true;
  }
}
