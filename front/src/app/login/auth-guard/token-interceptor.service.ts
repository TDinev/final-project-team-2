import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ) {
    const token = localStorage.getItem('accessToken') || '';
    const updatedRequest = request.clone({
      headers: request.headers.set('Authorization', `Bearer ${token}`)
    });

    return next.handle(updatedRequest);
  }
}
