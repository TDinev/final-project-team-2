import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/components/login/login.component';
import { HomeComponent } from './home/home.component';
import { SearchResultComponent } from './search-results/search-results.component';
import { ResultsResolverService } from './search-results/results-resolver.service';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AdminGuard } from './login/auth-guard/admin.guard';
import { AdminResolverService } from './admin/admin-resolver.service';
import { AuthGuard } from './login/auth-guard/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'search',
    resolve: { result: ResultsResolverService },
    runGuardsAndResolvers: 'paramsOrQueryParamsChange',
    component: SearchResultComponent,
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', canActivate: [AuthGuard, AdminGuard], component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'products', runGuardsAndResolvers: 'always', loadChildren: () =>
      import('./products/products.module').then(m => m.ProductsModule)
  },
  {
    path: 'admin',
    resolve: { result: AdminResolverService },
    canActivate: [AuthGuard, AdminGuard], component: AdminComponent
  },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
