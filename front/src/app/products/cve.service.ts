import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { CVEDto } from '../models/CVEs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CveService {

  baseCveUrl = `http://localhost:3000/cves`;

  constructor(
    private readonly http: HttpClient
  ) { }

  getCvesForCpe(cpeName: string): Observable<CVEDto> {
    return this.http.get<CVEDto>(`${this.baseCveUrl}/${cpeName}`);
  }

  getSingleCve(cveId: string): Observable<CVEDto> {
    return this.http.get<CVEDto>(`${this.baseCveUrl}/search/${cveId}`);
  }

  linkCveToProduct(cpeName: string, cve: CVEDto): Observable<void> {
    return this.http.post<void>(`${this.baseCveUrl}/${cpeName}`, cve);
  }

  getCvesFromLocalDb(cpeName: string): Observable<CVEDto[]> {
    return this.http.get<CVEDto[]>(`${this.baseCveUrl}/local/${cpeName}`);
  }

  getAllCvesFromLocalDb(): Observable<CVEDto[]> {
    return this.http.get<CVEDto[]>(`${this.baseCveUrl}/all`);
  }

  getCVEbyID(id: string) {
    return this.http.get<CVEDto>(`${this.baseCveUrl}/search/${id}`)
      .pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: HttpErrorResponse) {
    return of(`Product not found!`);
  }
}
