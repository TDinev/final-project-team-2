import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProductsService } from '../products.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CpeService } from '../cpe.service';


@Component({
  selector: 'app-create-new',
  templateUrl: './create-new.component.html',
  styleUrls: ['./create-new.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNewComponent implements OnInit {

  productCtrl: FormControl;
  vendorCtrl: FormControl;
  versionCtrl: FormControl;
  filteredProduct: Observable<any[]>;
  filteredVendors: Observable<any[]>;
  productsNames;
  productExists: number;
  product: string;
  vendorNames;
  vendorExists: number;
  vendor: string;
  version: string;
  cpeExists: string;

  constructor(
    private productsService: ProductsService,
    private cpeService: CpeService,
    private snackBar: MatSnackBar,
  ) {    }

  ngOnInit() {
    this.productCtrl = new FormControl();
    this.filteredProduct = this.productCtrl.valueChanges
      .pipe(
        map((product: string) => product.length >= 1 ? this.filterProducts(product) : [])
    );
    this.vendorCtrl = new FormControl();
    this.filteredVendors = this.vendorCtrl.valueChanges
      .pipe(
        map((vendor: string) => vendor.length >= 1 ? this.filterVendors(vendor) : [])
    );
  }

  private filterProducts(name: string) {
    this.productsNames = this.cpeService.productNames;
    return this.productsNames.getPrefix(name);
  }

  private filterVendors(name: string) {
    this.vendorNames = this.cpeService.vendorsNames;
    return this.vendorNames.getPrefix(name);
  }

  checkIfProductExists(productName: string) {
    const checkIfProductExistsObserver = {
      next: x => {
        this.productExists = x;
      },
      error: err => console.error(err)
    };
    this.cpeService.checkIfProductExists(productName).subscribe(checkIfProductExistsObserver);
  }

  checkIfVendorExists(vendorName: string) {
    const checkIfVendorExistsObserver = {
      next: x => {
        this.vendorExists = x;
      },
      error: err => console.error(err)
    };
    this.cpeService.checkIfVendorExists(vendorName).subscribe(checkIfVendorExistsObserver);
  }

  checkIfCpeExists(vendorName: string, productName: string, version: string) {
    const checkIfCpeExistsObserver = {
      next: x => {
        if (x) {
          this.createNewProduct();
          this.cpeExists = 'problemFixed';
        }else {
          this.cpeExists = 'problem';
          this.errorSnackBar('It seems the Product you are looking for cannot be found.Please check the version number or if the vendor and product correspond to each other.', 10000);
        }
      },
      error: err => console.error(err)
    };
    this.cpeService.checkIfCpeExists(vendorName,  productName, version).subscribe(checkIfCpeExistsObserver);
  }

  createNewProduct() {
    const newProduct = {
      vendor: this.vendor,
      product: this.product,
      version: this.version,
    };
    const createNewProductObserver = {
      next: x => {
        this.successSnackBar('New Product Saved!');
      },
      error: err => this.errorSnackBar('Error', 1500)
    };
    this.productsService.createNewProduct(newProduct).subscribe(createNewProductObserver);
  }

  successSnackBar(message: string) {
    this.snackBar.open(message, 'End', {
      duration: 1500,
      panelClass: ['green-snackbar']
    });
  }

  errorSnackBar(message: string, duration: number) {
    this.snackBar.open(message, 'End', {
      duration,
      panelClass: ['red-snackbar']
    });
  }

}
