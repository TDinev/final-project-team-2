import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { InventoryItemDto } from '../models/inventory-items';
import { CreateInventoryItemDto } from '../models/create-inventory-item';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  isAllProductsUsed = false;

  constructor(
    private readonly http: HttpClient
  ) { }

  baseUrl = `http://localhost:3000/inventory`;
  baseMailerUrl = `http://localhost:3000/sendMail`;

  getAllProducts(): Observable<InventoryItemDto[]> {
    return this.http.get<InventoryItemDto[]>(`${this.baseUrl}/all`);
  }

  linkCpeToProduct(inventoryItemId: number, cpeName: string): Observable<void> {
    return this.http.put<void>(`${this.baseUrl}/${inventoryItemId}/cpes/${cpeName}`, null);
  }

  breakLinkToCpe(inventoryItemId: number): Observable<InventoryItemDto> {
    return this.http.put<InventoryItemDto>(`${this.baseUrl}/${inventoryItemId}/breaklink`, null);
  }

  getProductById(inventoryItemId: number): Observable<InventoryItemDto> {
    return this.http.get<InventoryItemDto>(`${this.baseUrl}/${inventoryItemId}`);
  }

  deleteProduct(inventoryItemId: number): Observable<InventoryItemDto> {
    return this.http.delete<InventoryItemDto>(`${this.baseUrl}/${inventoryItemId}`);
  }

  newProduct(productData: CreateInventoryItemDto): Observable<CreateInventoryItemDto> {
    return this.http.post<CreateInventoryItemDto>(`${this.baseUrl}/new`, productData);
  }

  createNewProduct(newProduct: CreateInventoryItemDto): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/new`, newProduct);
  }

  getQuery(searchText: string): Observable<InventoryItemDto[] | string> {
    return this.http.get<InventoryItemDto[]>(`${this.baseUrl}`, {
      params: {
        keyword: searchText
      }
    }).pipe(
      catchError(this.errorHandler)
    );
  }

  alert(userName: string, cpe: string, cves: any[]) {
    const cveNames = cves.map(cve => cve.id);

    const body = { user: userName, cpe, cves: cveNames };
    return this.http.post(`${this.baseMailerUrl}`, body);
  }

  private errorHandler(error: HttpErrorResponse) {
    return of(`Product not found!`);
  }

}


