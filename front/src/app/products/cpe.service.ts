import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CpeDto } from '../models/CPEs';

@Injectable({
  providedIn: 'root'
})
export class CpeService {

  productNames;
  productNamesRaw;
  vendorsNames;
  vendorsNamesRaw;

  baseCpeUrl = `http://localhost:3000/cpes`;

  constructor(
    private readonly http: HttpClient
  ) { }

  getSuggestedCpes(inventoryItemId): Observable<CpeDto[]> {
    return this.http.get<CpeDto[]>(`${this.baseCpeUrl}/inventory/${inventoryItemId}/data`);
  }

  getAllProductNames(): Observable<string[]> {
    return this.http.get<string[]>(`${this.baseCpeUrl}/products`);
  }

  getAllVendorsNames(): Observable<string[]> {
    return this.http.get<string[]>(`${this.baseCpeUrl}/vendors`);
  }

  checkIfProductExists(productName: string): Observable<number> {
    return this.http.get<number>(`${this.baseCpeUrl}/product/${productName}`);
  }

  checkIfVendorExists(vendorName: string): Observable<number> {
    return this.http.get<number>(`${this.baseCpeUrl}/vendor/${vendorName}`);
  }

  checkIfCpeExists(vendorName: string, productName: string, version: string): Observable<CpeDto> {
    return this.http.get<CpeDto>(`${this.baseCpeUrl}/${vendorName}/${productName}/${version}`);
  }

}
