import { CveService } from "./cve.service";
import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CVEDto } from '../models/CVEs';

describe('ProductsService', () => {
  let httpClient;
  let service;

  beforeEach(() => {
    jest.clearAllMocks();

    httpClient = {
      get() { },
      post() { }
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [CveService]
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.inject(CveService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('getCvesForCpe()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const cpe = 'someCPE'
      const url = `http://localhost:3000/cves/${cpe}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getCvesForCpe(cpe).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('getSingleCve()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const cve = 'name';
      const url = `http://localhost:3000/cves/search/${cve}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getSingleCve(cve).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('linkCveToProduct()', () => {
    it('should call the httpClient.post() method once with correct parameters', done => {
      // Arrange
      const cpe = 'someCPE';
      const cve: CVEDto = {
        description: 'desc',
        id: 'id',
        lastModifiedDate: 'never',
        severity: 'none'
      };
      const url = `http://localhost:3000/cves/${cpe}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

      // Act & Assert
      service.linkCveToProduct(cpe, cve).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, cve);

        done();
      });
    });
  });
  describe('getCvesFromLocalDb()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const cpe = 'someCPE';
      const url = `http://localhost:3000/cves/local/${cpe}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getCvesFromLocalDb(cpe).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('getAllCvesFromLocalDb()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const url = `http://localhost:3000/cves/all`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllCvesFromLocalDb().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('getCVEbyID()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const id = 'someId'
      const url = `http://localhost:3000/cves/search/${id}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getCVEbyID(id).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
});
