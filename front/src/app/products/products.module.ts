import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { AllProductsComponent } from './all-products/all-products.component';
import { CreateNewComponent } from './create-new/create-new.component';
import { AssignedProductsComponent } from './assigned-products/assigned-products.component';
import { UnassignedProductsComponent } from './unassigned-products/unassigned-products.component';
import { MaterialModule } from '../material.module';
import { SingleCveComponent } from './single-cve/single-cve.component';
import { SingleProductComponent } from './single-product/single-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  declarations: [
    AllProductsComponent,
    CreateNewComponent,
    AssignedProductsComponent,
    UnassignedProductsComponent,
    SingleCveComponent,
    SingleProductComponent,
    DeleteProductComponent,
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    ScrollingModule,
  ],
  exports: [
    AllProductsComponent,
    SingleProductComponent,
    CreateNewComponent,
  ],
})
export class ProductsModule { }
