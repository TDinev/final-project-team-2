import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ProductsService } from './products.service';
import { of } from 'rxjs';
import { InventoryItemDto } from '../models/inventory-items';

describe('ProductsService', () => {
  let httpClient;
  let service;

  beforeEach(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
      delete() {}
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ProductsService]
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.inject(ProductsService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('getAllProducts()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const url = `http://localhost:3000/inventory/all`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllProducts().subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('linkCpeToProduct()', () => {
    it('should call the httpClient.put() method once with correct parameters', done => {
      // Arrange
      const inventoryItemId = 1;
      const cpeName = 'name';
      const url = `http://localhost:3000/inventory/${inventoryItemId}/cpes/${cpeName}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act & Assert
      service.linkCpeToProduct(inventoryItemId, cpeName).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, null);

        done();
      });
    });
  });
  describe('breakLinkToCpe()', () => {
    it('should call the httpClient.put() method once with correct parameters', done => {
      // Arrange
      const inventoryItemId = 1;
      const url = `http://localhost:3000/inventory/${inventoryItemId}/breaklink`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act & Assert
      service.breakLinkToCpe(inventoryItemId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, null);

        done();
      });
    });
  });
  describe('getProductById()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const inventoryItemId = 1;
      const url = `http://localhost:3000/inventory/${inventoryItemId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getProductById(inventoryItemId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('deleteProduct()', () => {
    it('should call the httpClient.del() method once with correct parameters', done => {
      // Arrange
      const inventoryItemId = 1;
      const url = `http://localhost:3000/inventory/${inventoryItemId}`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

      // Act & Assert
      service.deleteProduct(inventoryItemId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });
  });
  describe('createNewProduct()', () => {
    it('should call the httpClient.post() method once with correct parameters', done => {
      // Arrange
      const url = `http://localhost:3000/inventory/new`;
      const returnValue = of('return value');
      const product = new InventoryItemDto();

      const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

      // Act & Assert
      service.createNewProduct(product).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, product);

        done();
      });
    });
  });
  describe('getQuery()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const searchText = 'Test';
      const url = `http://localhost:3000/inventory`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getQuery(searchText).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, {params: {keyword: searchText}});

        done();
      });
    });
  });
  describe('alert()', () => {
    it('should call the httpClient.get() method once with correct parameters', done => {
      // Arrange
      const username = 'Test';
      const cpe = 'cpe';
      const cves = [{ id: 'cve' }];
      const cveNames = [cves[0].id];
      const body = { user: username, cpe, cves: cveNames };
      const url = `http://localhost:3000/sendMail`;
      const returnValue = of('return value');

      const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

      // Act & Assert
      service.alert(username, cpe, cves).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, body);

        done();
      });
    });
  });
});
  //   it('should return the result from the httpClient.get() method', () => {
  //     // Arrange
  //     const url = `http://localhost:3000/posts/all`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

  //     // Act
  //     const result = service.getAllPosts();

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('getSinglePost()', () => {
  //   it('should call the httpClient.get() method once with correct parameters', done => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

  //     // Act & Assert
  //     service.getSinglePost(id).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.get() method', () => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

  //     // Act
  //     const result = service.getSinglePost(id);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('newPost()', () => {
  //   it('should call the httpClient.post() method once with correct parameters', done => {
  //     // Arrange
  //     const url = `http://localhost:3000/posts`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
  //     const post = new ShowPostDTO();

  //     // Act & Assert
  //     service.newPost(post).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url, post);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.post() method', () => {
  //     // Arrange
  //     const url = `http://localhost:3000/posts`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
  //     const post = new ShowPostDTO();

  //     // Act
  //     const result = service.newPost(post);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('editPost()', () => {
  //   it('should call the httpClient.put() method once with correct parameters', done => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
  //     const post = { id };

  //     // Act & Assert
  //     service.editPost(id, post as any).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url, post);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.put() method', () => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
  //     const post = { id };

  //     // Act
  //     const result = service.editPost(id, post as any);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('deletePost()', () => {
  //   it('should call the httpClient.delete() method once with correct parameters', done => {
  //     // Arrange

  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

  //     // Act & Assert
  //     service.deletePost(id).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.get() method', () => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}`;
  //     const returnValue = of('return value');

  //     const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

  //     // Act
  //     const result = service.deletePost(id);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('createPostVote()', () => {
  //   it('should call the httpClient.post() method once with correct parameters', done => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}/votes`;
  //     const returnValue = of('return value');
  //     const newVote = 'Like';

  //     const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
  //     const postVote = { vote: newVote };

  //     // Act & Assert
  //     service.createPostVote(id, newVote).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url, postVote);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.post() method', () => {
  //     // Arrange
  //     const id = 1;
  //     const url = `http://localhost:3000/posts/${id}/votes`;
  //     const returnValue = of('return value');
  //     const newVote = 'Like';

  //     const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
  //     const postVote = { vote: newVote };

  //     // Act
  //     const result = service.createPostVote(id, postVote);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

  // describe('updatePostVote()', () => {
  //   it('should call the httpClient.post() method once with correct parameters', done => {
  //     // Arrange
  //     const id = 1;
  //     const voteId = 1;
  //     const url = `http://localhost:3000/posts/${id}/votes/${voteId}`;
  //     const returnValue = of('return value');
  //     const newVote = 'Like';

  //     const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
  //     const postVote = { newPostVote: newVote };

  //     // Act & Assert
  //     service.updatePostVote(id, newVote, voteId).subscribe(() => {
  //       expect(spy).toHaveBeenCalledTimes(1);
  //       expect(spy).toHaveBeenCalledWith(url, postVote);

  //       done();
  //     });
  //   });

  //   it('should return the result from the httpClient.post() method', () => {
  //     // Arrange
  //     const id = 1;
  //     const voteId = 1;
  //     const url = `http://localhost:3000/posts/${id}/votes/${voteId}`;
  //     const returnValue = of('return value');
  //     const newPostVote = 'Like';

  //     const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
  //     const postVote = { newPostVote };

  //     // Act
  //     const result = service.updatePostVote(id, postVote, voteId);

  //     // Assert
  //     expect(result).toEqual(returnValue);
  //   });
  // });

