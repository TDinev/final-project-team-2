import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    baseCpeUrl = `http://localhost:3000/cpes`;
    baseCveUrl = `http://localhost:3000/cves`;
    baseAdminUrl = `http://localhost:3000/admin`;

    constructor(
        private readonly http: HttpClient
    ) { }

    checkIfEmptyCPE(): Observable<boolean> {
        return this.http.get<boolean>(`${this.baseCpeUrl}`);
    }


    updateCVEs(): Observable<void> {
        return this.http.get<void>(`${this.baseAdminUrl}/cves/refresh`);
    }

    populateCpes() {
        return this.http.get(`${this.baseCpeUrl}/file`);
    }

    checkIfUpdateConfigured(): Observable<boolean> {
        return this.http.get<boolean>(`${this.baseCveUrl}/update/status`);
    }

    autoUpdateCVEs(data: any) {
        return this.http.patch(`${this.baseCveUrl}/update`, data);
    }

    autoUpdateCPEs(data: any) {

        return this.http.patch(`${this.baseCpeUrl}/update`, data);
    }

    deleteCronCVE() {
        return this.http.delete(`${this.baseCveUrl}/update`);
    }

    deleteCronCPE() {
        return this.http.delete(`${this.baseCpeUrl}/update`);
    }
}
