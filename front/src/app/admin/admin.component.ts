import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from './admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  loadingUpdate: boolean;
  loadingLoad: boolean;
  cpesAvailable: boolean;

  isCheckedUpdate: boolean;

  isCheckedAlerts: boolean;

  time: string;

  editable = false;

  constructor(
    private readonly adminService: AdminService,
    private readonly activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => this.cpesAvailable = !data.result);
    this.adminService.checkIfUpdateConfigured().subscribe(data => this.isCheckedUpdate = data);
  }

  populate() {
    this.loadingLoad = true;
    this.adminService.populateCpes().subscribe(data => this.loadingLoad = false);
    this.cpesAvailable = true;
  }

  manualUpdate() {
    this.loadingUpdate = true;
    this.adminService.updateCVEs().subscribe(data => this.loadingUpdate = false);
  }

  sliderChnage(event) {

    if (!event.checked) {

      this.adminService.deleteCronCVE().subscribe();
      this.adminService.deleteCronCPE().subscribe();
    } else {
      this.editable = true;
    }
  }
  activateAutoUpdate() {

    const data = {
      name: `updateCVEs`,
      hour: this.time.substr(0, 2),
      minutes: this.time.substr(3, 2)
    };
    this.adminService.autoUpdateCVEs(data).subscribe();

    data.name = `updateCPEs`;
    this.adminService.autoUpdateCPEs(data).subscribe();

    this.editable = false;

  }

  sliderChnageAlerts(event) {

    if (!event.checked) {

    }
  }

}
