import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products/products.service';
import { Chart } from 'chart.js';
import { CVEDto } from '../models/CVEs';
import { Router } from '@angular/router';
import { CveService } from '../products/cve.service';
import { CpeService } from '../products/cpe.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  assignmentChart;
  severityChart;
  barChart;

  constructor(
    private productsService: ProductsService,
    private cveService: CveService,
    private cpeService: CpeService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.getAllProducts();
    this.getAllCvesFromLocalDb();
    this.createProductVendorGraph();
  }

  getAllProducts() {
    const getAllProductsObserver = {
      next: x => {
        this.createAssignmentGraph(x);
      },
      error: err => console.error(err)
    };
    this.productsService.getAllProducts().subscribe(getAllProductsObserver);
  }

  private createAssignmentGraph(allProducts) {
    const assigned = allProducts.filter(product => product.cpe !== null && product.cves.length !== 0).length;
    const unassigned = allProducts.length - assigned;
    this.assignmentChart = new Chart('assign', {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [
            assigned,
            unassigned,
          ],
          backgroundColor: [
            '#B150G6',
            '#B80F0A'
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'assigned',
          'unassigned',
        ]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Assigned/Unassigned Products'
        },
        animation: {
          animateScale: true,
          animateRotate: true
        },
        onClick: (evt, item) => {
          const firstPoint = this.assignmentChart.getElementAtEvent(evt)[0];
          this.router.navigate([`/products/${this.assignmentChart.data.labels[firstPoint._index]}-products`]);
        }
      }
    });
  }

  getAllCvesFromLocalDb() {
    const getAllCvesFromLocalDbObserver = {
      next: x => { this.createSeverityGraph(x); },
      error: err => console.error(err)
    };
    this.cveService.getAllCvesFromLocalDb().subscribe(getAllCvesFromLocalDbObserver);
  }

  private createSeverityGraph(allCves: CVEDto[]) {
    const low: number = allCves.filter((cve: CVEDto) => cve.severity === 'LOW').length;
    const medium: number = allCves.filter((cve: CVEDto) => cve.severity === 'MEDIUM').length;
    const high: number = allCves.filter((cve: CVEDto) => cve.severity === 'HIGH').length;
    this.severityChart = new Chart('severity', {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [
            low,
            medium,
            high,
          ],
          backgroundColor: [
            '#B150G6',
            '#F79D30',
            '#B80F0A',
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Low',
          'Medium',
          'High'
        ]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'CVE Severity'
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    });
  }

  private createProductVendorGraph() {
    this.barChart = new Chart('bar', {
      type: 'horizontalBar',
      data: {
        labels: ['Number of products and labels'],
        datasets: [{
          label: 'Products',
          backgroundColor: '#B150G6',
          borderColor: '#222021',
          borderWidth: 1,
          data: [
            this.cpeService.productNamesRaw.length,
          ]
        }, {
          label: 'Vendors',
          backgroundColor: '#B80F0A',
          borderColor: '#800000',
          data: [
            this.cpeService.vendorsNamesRaw.length,
          ]
        }]
      },
      options: {
        elements: {
          rectangle: {
            borderWidth: 2,
          }
        },
        responsive: true,
        legend: {
          position: 'right',
        },
        title: {
          display: true,
          text: 'Number of Products and Vendors'
        },
        scales: {
          xAxes: [{
            ticks: {
              beginAtZero: true
            }
          }],
          yAxes: [{
            ticks: {
              display: false
            }
          }]
        }
      }
    });
  }

}
