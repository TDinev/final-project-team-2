export class CVEDto {
    id: string;
    description: string;
    lastModifiedDate: string;
    severity: string;
  }
