export class CreateInventoryItemDto {
    product: string;
    vendor: string;
    version: string;
}
