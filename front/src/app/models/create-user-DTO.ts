export class CreateUserDTO {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    username: string;
}
