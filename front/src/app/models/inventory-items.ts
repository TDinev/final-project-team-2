import { CpeDto } from './CPEs';

export class InventoryItemDto {
    id: number;
    product: string;
    vendor: string;
    version: string;
    cpe: CpeDto;
    vulnerable: boolean;
}
