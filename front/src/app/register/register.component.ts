import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../login/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  constructor(
    private registerLoginService: LoginService,
    private snackBar: MatSnackBar,
  ) { }
  onSubmit(f: NgForm) {
    const registerObserver = {
      next: x => {
        this.snackBarModal('User created!');
        f.resetForm();
      },
      error: err => this.snackBarModal(err.error.message)
    };
    this.registerLoginService.register(f.value).subscribe(registerObserver);
  }

  private snackBarModal(message) {
    this.snackBar.open(message, '', {
      duration: 2000,
      panelClass: ['red-snackbar']
    });
  }
}
