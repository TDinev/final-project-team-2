# Xposed

## Summary 

Xposed is a system to automate the process of finding possible vulnerabilities in software products installed inside an organization.
It can recieve as input a list of software products (the inventory) in CSV format. The CSV should have three attributes: vendor, product, and version. Xposed retrieves the software inventory from a database.

To find possible vulnerabilities for the software products, the system employs the [CPE dictionary](https://nvd.nist.gov/cpe.cfm) and the [CVE feeds](https://nvd.nist.gov/download.cfm). First, it provides a list of CPE candidates that match a software product.  Once a CPE is assigned to a product, the system searches for CVEs that possibly match the assigned CPE. The system sends an email to the administrator with information about the user, CPE and CVEs that were assigned.

### Main Features

* Software inventory import from a database as well as from the web interface
* [CPE](https://nvd.nist.gov/cpe.cfm) and [CVE](https://cve.mitre.org/) support
* Automatic CPE download, unzip and parsing
* Automatic update of the CPE and CVE repositories
* Usage via a web interface
* Public search by CVE IDs and private search by CVE ID, CPE, product name or vendor
* Notifications of vulnerable software via email 
* Implemented with JavaScript, Nest.js and Angular


### Local Repositories 

To carry out the CPE matching Xposed does not query the public [CPE dictionary](https://nvd.nist.gov/cpe.cfm). Instead, it queries its _local database_ which contains a copy of CPE repositories. Having a local copy of the CPE allows a fast lookup when executing the CPE matching algorithm. 

#### Tasks 

To keep the local repositories updated, the system defines two tasks:

1. <b>Daily DB Update </b> This task is scheduled daily. It updates the local repositories (CPE and CVE) with the new entries fetched from the public repositories. It performs a search of CVE matches for the software products which already have a CPE assigned and adds new CPEs to the local repository.
2. <b>Populate DB </b> This task is used to populate the local CPE repository when it's empty.

 
### CPE Matching 

This module allows assigning a suitable [CPE](https://cpe.mitre.org/) to a software product. To do this, the system searches in the CPE collection (local copy of the [CPE dictionary](https://nvd.nist.gov/cpe.cfm)) for the _best candidates_ that could match that software product. The selection of the CPE candidates is performed by comparing the software's information (vendor, product, and version) with the CPE entries' attributes (vendor,product, and version).

Once the user has selected a CPE for that software product, Xposed relates the selected CPE to the software product. 

### CVE Matching 

Based on the product's CPE, the system query the NEST API for matching CVEs. 

After finding possible matches, they are related to the CPE and the software product in the database. Each CVE match contains four elements: 

* `cve_id` CVE Identifier 
* `cpe_entries` CPE entries of the affected software products. However, only the entries that are relevant for the product of the inventory.
* `severity` The severity of the CVE.
* `desription` The CVE's description.
* `lastMosifiedDate` The last date th CVE has been updated.


### Alerts 

This module generates an alert when a user confirms that a CVE is a match for a software product. The alerts represent the user that made the match, the CPE and the CVEs matched to it. In addition, this module sends notifications (via email) containing this information. 


## Run the system 

The following sections explain how to run Xposed and its requirements. 

```
git clone https://gitlab.com/danielahristova/final-project-team-2.git
```

### Requirements


1. Node.js
2. MySql / MariaDB
3. Angular CLI



## Usage 

The following sections explain how to use the main functionalities.

### Start 

To start Xposed, run in the server folder
```
npm install
```
```
npm run start:dev
```
```
npm run seed
```
to start the backend. The seed script will create an admin user. You can find the credentials in the seed.ts file.

To start the client, in the front folder run
```
npm install
```
```
ng s
``` 


### Log in 

Xposed is accessed via its web interface. Clcik on the Log In button to access the private part of the application.

### Populate Local Repositories

To populate the local CPE repository, click on the Administration tab of the main menu, and then in the Load CPEs admin menu press the Populate button. If this button is not available, it means that the repository is already populated.  

### Update Local Repositories

To manualy repopulate the local epositories, click on the Administration tab of the main menu, and then in the Manual CVEs Update press the Update button. 

### Daily Repository Update 

By default the auto update is disabled. To schedule a daily update of the CPEs and CVEs, go to the Administration tab of the main menu, and then in the Auto Update. Click the slider to activate the daily update and choose the time when the repositories will be updated. When clicking on the Save buttont the task is activated. 

### Dashboard

The home page for registered user is the Dashboard. It displays:
* chart with assinged/unassigned product with links to them. 
* chart with the number of products with each severity - low, medium and high. 
* chart with total number of vendors and products according to the CPE disctionary.

### Public Search

Non-registered user can see only the search page where he can search by CVE ID. The result will display the CVE details. When clicking on it, the table row will expand and show the matching CPEs for it.

### Search for users

Registered users can search the products in the inventory by product name and vendor, CPE or CVE. Wildcards are available.

### Read Inventory

To read the software inventory from the database, select Products tab.

### Add To Inventory

To add new software from the web interface, select Add Product in the main menu. In the first field start typing the name of the product. Autocomplete will help you select the correct name. Same will do on the Vendor name. If the Vendor doesn't match the product, the system will notify you and won't be able to add it.


### Assign CPE

To assign a CPE to a software product, go to Products in the main menu. On this page, you will see a list of software products. The ones with red icon are not assigned yet. Click on the product and the single page product will be displayed. The system will search CPEs that could match the software product. Once you selected a suitable CPE, press the button Link CPE. 

### Dismiss CPE

The relation between the CPE and the product can be also removed by clicking the yellow unlink icon. This will remove the relation between the software product and the CPE. The CVEs (if such were related) will still be assigned to that CPE in case of future linking with another product.

### Search CVE Matches

CVE matches for a software product are automatically loaded when CPE is linked.

### Process CVE Matches 

To match a CVE click on the checkbox and then the green link icon. New CVEs could be assigned when clicking on the Update CVEs button. Matching CVEs will be displayed again and the ones already assigned won't be duplicated. 


### Registrer Users

To register new user, click on the New user tab of the main menu. Only administrators can register new users.


## Contact 

[Daniela Hristova](https://gitlab.com/danielahristova)

[Todor Dinev](https://gitlab.com/TDinev)
